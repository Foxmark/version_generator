<?php
$animals = [
    'Dog',
    'Turtle',
    'Rabbit',
    'Parrot',
    'Cat',
    'Kitten',
    'Goldfish',
    'Mouse',
    'Hamster',
    'Cow',
    'Rabbit',
    'Ducks',
    'Shrimp',
    'Pig',
    'Goat',
    'Crab',
    'Deer',
    'Bee',
    'Sheep',
    'Fish',
    'Turkey',
    'Dove',
    'Chicken',
    'Horse',
    'Crow',
    'Peacock',
    'Dove',
    'Sparrow',
    'Goose',
    'Stork',
    'Pigeon',
    'Turkey',
    'Hawk',
    'Eagle',
    'Raven',
    'Parrot',
    'Flamingo',
    'Seagull',
    'Ostrich',
    'Swallow',
    'Black Bird',
    'Penguin',
    'Robin',
    'Swan',
    'Owl',
    'Woodpecker',
    'Squirrel',
    'Dog',
    'Chimpanzee',
    'Ox',
    'Lion',
    'Panda',
    'Walrus',
    'Otter',
    'Mouse',
    'Kangaroo',
    'Goat',
    'Horse',
    'Monkey',
    'Cow',
    'Koala',
    'Mole',
    'Elephant',
    'Leopard',
    'Hippopotamus',
    'Giraffe',
    'Fox',
    'Coyote',
    'Hedgehong',
    'Sheep',
    'Deer',
    'Giraffe',
    'Woodpecker',
    'Camel',
    'Starfish',
    'Koala',
    'Alligator',
    'Owl',
    'Tiger',
    'Bear',
    'Blue Whale',
    'Coyote',
    'Chimpanzee',
    'Raccoon',
    'Lion',
    'Arctic Wolf',
    'Crocodile',
    'Dolphin',
    'Elephant',
    'Squirrel',
    'Snake',
    'Kangaroo',
    'Hippopotamus',
    'Elk',
    'Fox',
    'Gorilla',
    'Bat',
    'Hare',
    'Toad',
    'Frog',
    'Deer',
    'Rat',
    'Badger',
    'Lizard',
    'Mole',
    'Hedgehog',
    'Otter',
    'Reindeer',
    'Crab',
    'Fish',
    'Seal',
    'Octopus',
    'Shark',
    'Seahorse',
    'Walrus',
    'Starfish',
    'Whale',
    'Penguin',
    'Jellyfish',
    'Squid',
    'Lobster',
    'Pelican',
    'Clams',
    'Seagull',
    'Dolphin',
    'Shells',
    'Cormorant',
    'Otter',
    'Pelican',
    'Sea Turtle',
    'Sea Lion',
    'Coral',
    'Moth',
    'Bee',
    'Butterfly',
    'Spider',
    'Ant',
    'Dragonfly',
    'Fly',
    'Mosquito',
    'Grasshopper',
    'Beetle',
    'Cockroach',
    'Centipede',
    'Worm',
    'Louse'
];

$adjectives = [
    'attractive',
    'bald',
    'beautiful',
    'chubby',
    'clean',
    'dazzling',
    'drab',
    'elegant',
    'fancy',
    'fit',
    'flabby',
    'glamorous',
    'gorgeous',
    'handsome',
    'long',
    'magnificent',
    'muscular',
    'plain',
    'plump',
    'quaint',
    'scruffy',
    'shapely',
    'short',
    'skinny',
    'stocky',
    'ugly',
    'unkempt',
    'unsightly',
    'ashy',
    'black',
    'blue',
    'gray',
    'green',
    'icy',
    'lemon',
    'mango',
    'orange',
    'purple',
    'red',
    'salmon',
    'white',
    'yellow',
    'aggressive',
    'agreeable',
    'ambitious',
    'brave',
    'calm',
    'delightful',
    'eager',
    'faithful',
    'gentle',
    'happy',
    'jolly',
    'kind',
    'lively',
    'nice',
    'obedient',
    'polite',
    'proud',
    'silly',
    'thankful',
    'victorious',
    'witty',
    'wonderful',
    'zealous',
    'angry',
    'bewildered',
    'clumsy',
    'defeated',
    'embarrassed',
    'fierce',
    'grumpy',
    'helpless',
    'itchy',
    'jealous',
    'lazy',
    'mysterious',
    'nervous',
    'obnoxious',
    'panicky',
    'pitiful',
    'repulsive',
    'scary',
    'thoughtless',
    'uptight',
    'worried',
    'crashing',
    'deafening',
    'echoing',
    'faint',
    'harsh',
    'hissing',
    'howling',
    'loud',
    'melodic',
    'noisy',
    'purring',
    'quiet',
    'rapping',
    'raspy',
    'rhythmic',
    'screeching',
    'shrilling',
    'squeaking',
    'thundering',
    'tinkling',
    'wailing',
    'whining',
    'whispering'
];

// you can use count($animal)-1 but this is faster and I won't be adding more elements for now
$animal_count    = 145;
$adjective_count = 108;

// get one random element from each array
$animal          = $animals[rand(0, $animal_count)];
$adjective       = $adjectives[rand(0, $adjective_count)];

// create text and slug
$string          = ucfirst($adjective) . ' ' . ucfirst($animal);
$slug            = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Version Generator 1.0</title>
    <style>
    </style>
</head>
<body>
<div class="card-deck mt-3 mb-3 text-center">
    <p><?php echo $string; ?></p>
    <p><?php echo $slug; ?></p>
</div>
</body>
</html>